import React from "react";

import TodoItem from "./todo-item";
import TodoSection from "./todo-section";
import ClickToTextInput from "./form/click-to-text-input";

import { Badge, Button } from "reactstrap";
import { DragDropContext } from "react-beautiful-dnd";

function TodoList(props) {
  const { items = [] } = props;

  const todoItems = items
    .filter(item => !item.done)
    .sort((a, b) => a.priority - b.priority);
  const doneItems = items
    .filter(item => item.done)
    .sort((a, b) => a.priority - b.priority);

  const addItem = task => {
    return props.addItem({ task, todo_id: props.todo.id });
  };

  const handleDeleteClick = evt => {
    evt.preventDefault();
    props.deleteTodo(props.todo.id);
  };

  const onDragEnd = result => {
    // Dropped outside or same position
    if (
      !result.destination ||
      (result.destination.droppableId === result.source.droppableId &&
        result.destination.index === result.source.index)
    ) {
      return;
    }

    const item = items.find(el => el.id === result.draggableId);

    // Item dragged into other list than its?
    const toggleDone =
      (!item.done && result.destination.droppableId === "done") ||
      (item.done && result.destination.droppableId === "todo");

    let priority = result.destination.index;

    // weirdness react-beautiful-dnd: if item dragged into other list, index is 0-based. 1-based otherwise
    if (toggleDone) {
      priority++;
    }

    props.updateItem({
      ...item,
      priority,
      done: toggleDone ? !item.done : item.done
    });
  };

  return (
    <div className="card" style={{ marginBottom: "2em" }}>
      <div className="card-header d-flex justify-content-between align-items-center">
        <h4 style={{ margin: 0 }}>
          {props.todo.title} <Badge>{todoItems.length}</Badge>
        </h4>
        <Button color="danger" onClick={handleDeleteClick}>
          Delete
        </Button>
      </div>

      <div className="card-body">
        <DragDropContext onDragEnd={onDragEnd}>
          {todoItems.length + doneItems.length !== 0 ? (
            <TodoSection name="todo">
              {todoItems.map(item => (
                <TodoItem
                  item={item}
                  key={item.id}
                  update={props.updateItem}
                  delete={props.deleteItem}
                />
              ))}
            </TodoSection>
          ) : null}

          <ClickToTextInput onSubmit={addItem}>
            <Button color="primary" outline className="btn-block">
              Add task
            </Button>
          </ClickToTextInput>

          {items.length + doneItems.length !== 0 ? (
            <div>
              <div
                className="card-title d-flex justify-content-between align-items-center"
                style={{ marginTop: "1.5em" }}
              >
                <h5>
                  Done <Badge>{doneItems.length}</Badge>
                </h5>
              </div>

              <TodoSection name="done">
                {doneItems.map(item => (
                  <TodoItem
                    item={item}
                    key={item.id}
                    update={props.updateItem}
                    delete={props.deleteItem}
                  />
                ))}
              </TodoSection>
            </div>
          ) : null}
        </DragDropContext>
      </div>
    </div>
  );
}

export default TodoList;
