import React from "react";

import { Draggable } from "react-beautiful-dnd";

import { Button } from "reactstrap";
import ClickToTextInput from "components/form/click-to-text-input";

function TodoItem(props) {
  const { item } = props;

  const updateTodoTitle = task => props.update({ ...props.item, task });

  const handleToggleDone = () => {
    const newItem = { ...item, done: !item.done, priority: 1 };
    props.update(newItem);
  };

  const handleDelete = () => {
    props.delete(item);
  };

  const getItemStyle = (isDragging, draggableStyle) => ({
    borderRadius: isDragging ? 0 : null,
    ...draggableStyle
  });

  return (
    <Draggable draggableId={item.id} index={item.priority}>
      {(provided, snapshot) => (
        <div
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          style={getItemStyle(
            snapshot.isDragging,
            provided.draggableProps.style
          )}
          className="list-group-item d-flex justify-content-between align-items-center"
        >
          {props.children}

          <ClickToTextInput onSubmit={updateTodoTitle} value={item.task}>
            <span>{item.task}</span>
          </ClickToTextInput>

          <div>
            <Button
              outline
              color={item.done ? "warning" : "success"}
              size="sm"
              onClick={handleToggleDone}
            >
              {item.done ? "KO" : "OK"}
            </Button>
            &nbsp;
            <Button outline color="danger" size="sm" onClick={handleDelete}>
              X
            </Button>
          </div>
        </div>
      )}
    </Draggable>
  );
}

export default TodoItem;
