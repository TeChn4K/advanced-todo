import React from "react";
import TodoItem from "./index";
import { DragDropContext } from "react-beautiful-dnd";

import { render, fireEvent, cleanup, prettyDOM } from "react-testing-library";

describe("TodoItem", () => {
  // There are some errors with react-beautiful-dnd, we're ignoring them...
  console.error = jest.fn();

  let props;
  let mountedComponent;
  const component = () => {
    if (!mountedComponent) {
      mountedComponent = render(
        <DragDropContext>
          <TodoItem {...props} />
        </DragDropContext>
      );
    }

    return mountedComponent;
  };

  beforeEach(() => {
    mountedComponent = undefined;
  });

  afterEach(() => {
    cleanup();
  });

  const onUpdateMock = jest.fn();
  const onDeleteMock = jest.fn();
  props = {
    item: { id: "1", task: "yoyo", done: false },
    update: onUpdateMock,
    delete: onDeleteMock
  };

  it("displays his title", () => {
    const { getByText } = component();
    expect(getByText("yoyo")).not.toBeNull();
  });

  it("calls update function on click OK/KO", () => {
    const { getByText } = component();

    fireEvent.click(getByText("OK"));
    expect(onUpdateMock).toHaveBeenCalledWith(
      expect.objectContaining({
        done: true
      })
    );
  });

  it("calls delete function on click X", () => {
    const { getByText } = component();

    fireEvent.click(getByText("X"));
    expect(onDeleteMock).toHaveBeenCalled();
  });
});
