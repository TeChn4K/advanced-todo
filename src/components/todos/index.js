import React from "react";

import { Row, Col, Button } from "reactstrap";

import TodoList from "../../components/todo-list";
import ClickToTextInput from "../../components/form/click-to-text-input";

function Todos(props) {
  const { todos, items } = props;

  if (!todos || !items) {
    return <div>Loading ...</div>;
  }

  const count = Object.keys(todos).length;
  return (
    <>
      <Row>
        {Object.values(todos).map(todo => (
          <Col
            key={todo.id}
            md={{ size: count > 1 ? 6 : 12 }}
            lg={{ size: count > 2 ? 4 : null }}
          >
            <TodoList
              todo={todo}
              items={items[todo.id]}
              deleteTodo={props.deleteTodo}
              addItem={props.addItem}
              deleteItem={props.deleteItem}
              updateItem={props.updateItem}
            />
          </Col>
        ))}
      </Row>

      <ClickToTextInput onSubmit={props.addTodo}>
        <Button color="primary">New list</Button>
      </ClickToTextInput>
    </>
  );
}

export default Todos;
