import React, { useState, useEffect, useLayoutEffect, useRef } from "react";

import AuthService from "../../helpers/auth";

import { Alert, Button } from "reactstrap";

function TokenExpire({ displayMin }) {
  // Get token expiration date, or a past date if false returned
  let expirationDate = AuthService.getExpirationDate() || new Date(0);

  const getRemainingTime = deadline => {
    const currentTime = new Date().getTime();
    return Math.floor((deadline.getTime() - currentTime) / 1000);
  };

  const [timeLeft, setTimeLeft] = useState(getRemainingTime(expirationDate));

  let last = 0;
  let timeoutId = useRef();
  let frameId = useRef();

  const handleRefreshClick = () => {
    AuthService.refreshToken();
  };

  const tick = now => {
    const time = getRemainingTime(expirationDate);

    if (time <= 0) {
      stop();
      setTimeLeft(0);
    } else {
      // Each second only!
      if (now - last >= 1000) {
        last = now;

        setTimeLeft(time);
      }

      loop();
    }
  };

  const loop = () => {
    frameId.current = requestAnimationFrame(tick);
  };

  const start = () => {
    const expireTime = getRemainingTime(expirationDate);

    // Not time to display yet
    if (expireTime > displayMin) {
      timeoutId.current = setTimeout(loop, (expireTime - displayMin) * 1000);
    }
    // Have to show right now, unless expired
    else if (displayMin >= expireTime && expireTime > 0) {
      loop();
    }
  };

  const stop = () => {
    if (timeoutId.current) {
      clearTimeout(timeoutId.current);
      timeoutId.current = null;
    }
    cancelAnimationFrame(frameId.current);
  };

  const restart = () => {
    stop();
    // Get token expiration date, or a past date if false returned
    expirationDate = AuthService.getExpirationDate() || new Date(0);
    setTimeLeft(getRemainingTime(expirationDate));
    start();
  };

  useEffect(() => {
    const callbackId = AuthService.onNewToken(restart);
    return () => AuthService.unNewToken(callbackId);
  }, []);

  useLayoutEffect(() => {
    start();
    return () => stop();
  }, [displayMin]);

  if (displayMin >= timeLeft && timeLeft > 0) {
    const minLeft = Math.floor(timeLeft / 60);
    let secLeft = timeLeft - minLeft * 60;

    if (secLeft.toString().length === 1) {
      secLeft = `0${secLeft}`;
    }

    return (
      <Alert color="warning">
        Your session will expire in {minLeft}:{secLeft}
        <Button
          color="link"
          className="alert-link"
          onClick={handleRefreshClick}
        >
          <i>Extend now!</i>
        </Button>
      </Alert>
    );
  }

  return null;
}

TokenExpire.defaultProps = {
  displayMin: 600
};

export default TokenExpire;
