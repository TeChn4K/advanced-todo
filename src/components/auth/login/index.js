import React, { useState } from "react";
import PropTypes from "prop-types";

import {
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  Button,
  Form,
  FormGroup,
  Label,
  Input
} from "reactstrap";

import { capitalize } from "lodash";

function LoginForm(props) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // Usefull to call functions by their names
  const functionNameHash = {
    setEmail,
    setPassword
  };

  const handleChange = evt => {
    functionNameHash[`set${capitalize(evt.target.name)}`](evt.target.value);
  };

  const handleSubmit = evt => {
    evt.preventDefault();
    props.onSubmit({ email, password });
  };

  return (
    <Row>
      <Col
        sm={{ size: 10, offset: 1 }}
        md={{ size: 8, offset: 2 }}
        lg={{ size: 6, offset: 3 }}
      >
        <Card>
          <CardBody>
            <CardTitle>Login</CardTitle>

            <Form onSubmit={handleSubmit}>
              <FormGroup>
                <Label for="emailInput">Email</Label>
                <Input
                  type="email"
                  name="email"
                  id="emailInput"
                  value={email}
                  onChange={handleChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="passwordInput">Password</Label>
                <Input
                  type="password"
                  name="password"
                  id="passwordInput"
                  value={password}
                  onChange={handleChange}
                />
              </FormGroup>
              <Button type="submit" color="primary" type="submit">
                Submit
              </Button>
            </Form>
          </CardBody>
        </Card>
      </Col>
    </Row>
  );
}

LoginForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
};

export default LoginForm;
