import React from "react";
import { render, fireEvent, cleanup, prettyDOM } from "react-testing-library";
import LoginAuth from "./index";

describe("LoginAuth", () => {
  let props;
  let mountedComponent;
  const component = () => {
    if (!mountedComponent) {
      mountedComponent = render(<LoginAuth {...props} />);
    }

    return mountedComponent;
  };

  beforeEach(() => {
    mountedComponent = undefined;
  });

  afterEach(() => {
    cleanup();
  });

  const onSubmitMock = jest.fn();
  props = {
    onSubmit: onSubmitMock
  };

  describe("When form is submited", () => {
    it("calls the onSubmit prop function with email/password", () => {
      const { getByText, getByLabelText } = component();

      fireEvent.change(getByLabelText("Email"), {
        target: { value: "test@test.com" }
      });
      fireEvent.change(getByLabelText("Password"), {
        target: { value: "passtest" }
      });
      fireEvent.click(getByText("Submit"));

      expect(onSubmitMock).toHaveBeenCalledWith(
        expect.objectContaining({
          email: "test@test.com",
          password: "passtest"
        })
      );
    });
  });
});
