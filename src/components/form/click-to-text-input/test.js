import React from "react";
import ClickToTextInput from "./index";

import { render, fireEvent, cleanup, prettyDOM } from "react-testing-library";

describe("ClickToTextInput", () => {
  let props;
  let mountedComponent;
  const component = () => {
    if (!mountedComponent) {
      mountedComponent = render(
        <ClickToTextInput {...props}>
          <button>myButton</button>
        </ClickToTextInput>
      );
    }

    return mountedComponent;
  };

  beforeEach(() => {
    props = {
      onSubmit: undefined,
      label: undefined,
      value: undefined,
      placeholder: undefined
    };
    mountedComponent = undefined;
  });

  afterEach(() => {
    cleanup();
  });

  it("display child by default and form on click", () => {
    const { container, queryByText, getByText } = component();

    fireEvent.click(getByText("myButton"));

    expect(queryByText("myButton")).toBeNull();
    expect(container.querySelector("form")).not.toBeNull();
  });

  describe("When prop `onSubmit` is defined", () => {
    const onSubmitMock = jest.fn();

    beforeEach(() => {
      props.onSubmit = onSubmitMock;
      props.label = "myLabel"; // Only used to retrieve the input wwith getByLabelText("myLabel")
      props.submitText = "submit";

      const { getByText, getByLabelText } = component();
      fireEvent.click(getByText("myButton"));

      fireEvent.change(getByLabelText("myLabel"), {
        target: { value: "newValue" }
      });

      fireEvent.click(getByText("submit"));
    });

    it("Function is called on submit, and it receives values", () => {
      expect(onSubmitMock).toBeCalled();
      expect(onSubmitMock.mock.calls[0][0]).toBe("newValue");
    });
  });

  describe("When prop `label` is defined", () => {
    beforeEach(() => {
      props.label = "testlabel";
    });

    it("label is rendered", () => {
      const { getByText } = component();
      fireEvent.click(getByText("myButton"));

      expect(getByText("testlabel")).not.toBeNull();
    });
  });

  describe("When prop `placeholder` is defined", () => {
    beforeEach(() => {
      props.placeholder = "myPlaceholderText";
    });

    it("placeholder is rendered", () => {
      const { getByText, queryByPlaceholderText } = component();
      fireEvent.click(getByText("myButton"));

      expect(queryByPlaceholderText("myPlaceholderText")).not.toBeNull();
    });
  });

  describe("When prop `value` is defined", () => {
    beforeEach(() => {
      props.value = "myValue";
      props.label = "myLabel"; // Only used to retrieve the input wwith getByLabelText("myLabel")
    });

    it("the input text displays this value first", () => {
      const { getByText, getByLabelText } = component();
      fireEvent.click(getByText("myButton"));

      expect(getByLabelText("myLabel").value).toEqual("myValue");
    });
  });
});
