import React, { useState } from "react";

import PropTypes from "prop-types";
import { Button, ButtonGroup, Form, Input, Col, Row } from "reactstrap";
import uniqueId from "lodash/uniqueId";

function ClickToTextInput(props) {
  const [text, setText] = useState(props.value);
  const [isDisplay, setIsDisplay] = useState(false);

  const uid = uniqueId("ctti-");

  const handleToogle = () => {
    setIsDisplay(!isDisplay);
  };

  const handleChange = evt => {
    setText(evt.target.value);
  };

  const handleSubmit = evt => {
    evt.preventDefault();

    props.onSubmit(text);

    handleToogle();
    setText("");
  };

  let toRender;

  if (!isDisplay) {
    toRender = (
      <div>
        {React.cloneElement(props.children, {
          onClick: handleToogle
        })}
      </div>
    );
  } else {
    toRender = (
      <Form onSubmit={handleSubmit}>
        <Row noGutters>
          <Col className="mb-2 mb-md-0 mr-md-1">
            {props.label ? (
              <label className="sr-only" htmlFor={uid}>
                {props.label}
              </label>
            ) : null}
            <Input
              type="text"
              name="text"
              bsSize="sm"
              id={uid}
              placeholder={props.placeholder}
              value={text}
              onChange={handleChange}
            />
          </Col>
          <Col md={{ size: "auto" }}>
            <ButtonGroup>
              <Button color="primary" size="sm" type="submit">
                {props.submitText}
              </Button>
              <Button color="secondary" size="sm" onClick={handleToogle}>
                {props.cancelText}
              </Button>
            </ButtonGroup>
          </Col>
        </Row>
      </Form>
    );
  }

  return toRender;
}

ClickToTextInput.propTypes = {
  onSubmit: PropTypes.func,
  value: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  submitText: PropTypes.string,
  cancelText: PropTypes.string
};

ClickToTextInput.defaultProps = {
  submitText: "Save",
  cancelText: "Cancel",
  value: ""
};

export default ClickToTextInput;
