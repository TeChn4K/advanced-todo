import React from "react";

import { Droppable } from "react-beautiful-dnd";

function TodoSection(props) {
  return (
    <Droppable droppableId={props.name}>
      {(provided, snapshot) => (
        <div
          ref={provided.innerRef}
          style={{
            minHeight: "57px",
            backgroundColor: snapshot.isDraggingOver
              ? "rgba(0,0,0,.2)"
              : "rgba(0,0,0,.03)",
            borderRadius: "0.25em",
            marginBottom: "10px"
          }}
          className="list-group"
        >
          {props.children}
          {provided.placeholder}
        </div>
      )}
    </Droppable>
  );
}

export default TodoSection;
