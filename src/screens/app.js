import { hot } from "react-hot-loader/root";

import React from "react";
import { BrowserRouter, Route, Redirect, Switch } from "react-router-dom";
import { Container } from "reactstrap";

import TodosScreen from "./todos";
import LoginScreen from "./login";
import LogoutScreen from "./logout";
import Navapp from "../components/navapp";
import TokenExpire from "../components/auth/token-expire";

import AuthService from "../helpers/auth";

const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        AuthService.isLogged() ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
};

function App() {
  return (
    <BrowserRouter>
      <div>
        <Navapp />
        <Container style={{ paddingTop: "0.6rem" }}>
          <TokenExpire displayMin="1800" />
          <Switch>
            <PrivateRoute path="/" exact component={TodosScreen} />
            <Route path="/login" exact component={LoginScreen} />
            <Route path="/logout" exact component={LogoutScreen} />
            <Redirect to="/" />
          </Switch>
        </Container>
      </div>
    </BrowserRouter>
  );
}

export default hot(App);
