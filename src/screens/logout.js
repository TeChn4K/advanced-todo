import React from "react";

import LogoutContainer from "containers/logout";

function LogoutScreen() {
  return <LogoutContainer />;
}

export default LogoutScreen;
