import React from "react";

import LoginContainer from "../containers/login";

function LoginScreen() {
  return (
    <>
      {/*Login title here*/}
      <LoginContainer />
    </>
  );
}

export default LoginScreen;
