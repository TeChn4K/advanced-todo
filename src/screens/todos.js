import React from "react";

import TodosContainer from "../containers/todos";

function TodosScreen() {
  return (
    <>
      {/* <h1>Todos</h1> */}
      <TodosContainer />
    </>
  );
}

export default TodosScreen;
