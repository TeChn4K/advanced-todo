import React from "react";
import { withRouter } from "react-router";
import LoginForm from "components/auth/login/";
import AuthService from "helpers/auth";
import { Alert } from "reactstrap";

function LoginContainer(props) {
  const onSubmit = async ({ email, password }) => {
    await AuthService.login(email, password);
    const { from } = props.location.state || { from: { pathname: "/" } };
    props.history.push(from);
  };

  return (
    <>
      {props.location.state ? (
        <Alert color="danger">Please login to continue</Alert>
      ) : null}
      <LoginForm onSubmit={onSubmit} />
    </>
  );
}

export default withRouter(LoginContainer);
