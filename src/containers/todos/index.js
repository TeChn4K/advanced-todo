import React, { useState, useEffect } from "react";

import TodosApiHelper from "helpers/api/todos";
import todoItemsApiHelper from "helpers/api/todoItems";
import TodosService from "helpers/todos";

import Todos from "components/todos";

function TodosContainer(props) {
  const [todos, setTodos] = useState(null);
  const [items, setItems] = useState(null);

  useEffect(() => {
    const fetchTodos = async () => {
      // try {
      const data = await TodosApiHelper.fetch();

      const tmptodos = {};
      const tmpitems = {};

      data.map(todo => {
        tmpitems[todo.id] = todo.todo_items;
        delete todo.todo_items;
        tmptodos[todo.id] = todo;
      });

      setTodos(tmptodos);
      setItems(tmpitems);
    };

    fetchTodos();
  }, []);

  const addTodo = async title => {
    // We have to wait for todo `id`
    const response = await TodosApiHelper.add({ title });

    const todo = response[0];

    setTodos({
      ...todos,
      [todo.id]: todo
    });
    setItems({
      ...items,
      [todo.id]: []
    });

    return todo;
  };

  const deleteTodo = async id => {
    // toString is required because of a Babel bug ...
    const { [id.toString()]: _, ...otherItems } = items;
    const { [id.toString()]: __, ...otherTodos } = todos;

    setItems(otherItems);
    setTodos(otherTodos);

    return await TodosApiHelper.delete(id);
  };

  const addItem = async item => {
    // We have to wait for item `id` and `priority`
    const response = await todoItemsApiHelper.add(item);

    const newItem = response[0];
    const todoId = todos[item.todo_id].id;

    setItems({
      ...items,
      [todoId]: TodosService.addItemTo(items[todoId], newItem)
    });

    return newItem;
  };

  const updateItem = async item => {
    // Retrieve old item and refresh items array
    let oldItem = items[item.todo_id].find(el => el.id === item.id);

    let listUpdated = TodosService.updateItemFrom(items[item.todo_id], item);

    // Manage priorities
    if (item.priority !== oldItem.priority || item.done !== oldItem.done) {
      listUpdated = TodosService.updateItemsPriority(
        listUpdated,
        item,
        oldItem
      );
    }

    setItems({
      ...items,
      [item.todo_id]: listUpdated
    });

    return await todoItemsApiHelper.update(item);
  };

  const deleteItem = async item => {
    setItems({
      ...items,
      [item.todo_id]: TodosService.deleteItemFrom(items[item.todo_id], item)
    });

    return await todoItemsApiHelper.delete(item.id);
  };

  return (
    <Todos
      todos={todos}
      items={items}
      deleteTodo={deleteTodo}
      addItem={addItem}
      addTodo={addTodo}
      deleteItem={deleteItem}
      updateItem={updateItem}
    />
  );
}

export default TodosContainer;
