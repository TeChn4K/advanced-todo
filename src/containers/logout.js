import React, { useEffect } from "react";
import { withRouter } from "react-router";
import AuthService from "helpers/auth";

function LogoutContainer(props) {
  useEffect(() => {
    AuthService.logout();
    props.history.push("/login");
  }, []);

  return null;
}

export default withRouter(LogoutContainer);
