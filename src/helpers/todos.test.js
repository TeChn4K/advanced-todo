import React from "react";
import TodosService from "./todos";

describe("TodosService", () => {
  it("manages priorities correctly when an item is added", () => {
    const list = [
      {
        id: 1,
        done: false,
        priority: 1
      },
      {
        id: 2,
        done: true,
        priority: 1
      },
      {
        id: 3,
        done: false,
        priority: 2
      }
    ];
    const item = { id: 4, done: false, priority: 1 };

    const result = TodosService.addItemTo(list, item);

    expect(result.length).toEqual(4);
    expect(result.find(el => el.id === 2)).toMatchObject({
      done: true,
      priority: 1
    });
    expect(result.find(el => el.id === 4)).toMatchObject({
      done: false,
      priority: 1
    });
    expect(result.find(el => el.id === 1)).toMatchObject({
      done: false,
      priority: 2
    });
    expect(result.find(el => el.id === 3)).toMatchObject({
      done: false,
      priority: 3
    });
  });

  it("manages priorities correctly when an item is removed", () => {
    const list = [
      {
        id: 1,
        done: false,
        priority: 1
      },
      {
        id: 2,
        done: true,
        priority: 1
      },
      {
        id: 3,
        done: false,
        priority: 2
      }
    ];
    const item = { id: 1 };

    const result = TodosService.deleteItemFrom(list, item);

    expect(result.length).toEqual(2);
    expect(result.find(el => el.id === 2)).toMatchObject({
      done: true,
      priority: 1
    });
    expect(result.find(el => el.id === 3)).toMatchObject({
      done: false,
      priority: 1
    });
  });

  it("updates item's properties when updated (others than priority)", () => {
    const list = [
      {
        id: 1,
        task: "title 1",
        done: false,
        priority: 1
      },
      {
        id: 2,
        task: "title 2",
        done: true,
        priority: 1
      }
    ];
    const item = { id: 1, task: "new title 1" };

    const result = TodosService.updateItemFrom(list, item);

    expect(result.length).toEqual(2);
    expect(result.find(el => el.id === 1)).toMatchObject({
      task: "new title 1"
    });
    expect(result.find(el => el.id === 2)).toMatchObject({
      task: "title 2"
    });
  });

  describe("When item's priority have changed", () => {
    let list;

    beforeEach(() => {
      list = [
        {
          id: 1,
          done: false,
          priority: 1
        },
        {
          id: 2,
          done: false,
          priority: 2
        },
        {
          id: 3,
          done: false,
          priority: 3
        },
        {
          id: 4,
          done: true,
          priority: 1
        },
        {
          id: 5,
          done: true,
          priority: 2
        },
        {
          id: 6,
          done: true,
          priority: 3
        }
      ];
    });

    describe("And `done` flag have not changed", () => {
      it("doesn't update any other items with different flag", () => {
        const oldItem = list.find(el => el.id === 4);
        const item = { ...oldItem, priority: 2 };

        const result = TodosService.updateItemsPriority(list, item, oldItem);

        list
          .filter(el => el.done === false)
          .map(old => {
            expect(result.find(next => next.id === old.id)).toMatchObject(old);
          });
      });

      it("shifts down other items priority when item priority shift up", () => {
        const oldItem = list.find(el => el.id === 4);
        const item = { ...oldItem, priority: 2 };

        const result = TodosService.updateItemsPriority(list, item, oldItem);

        expect(result.find(el => el.id === 5)).toMatchObject({
          priority: 1
        });
        expect(result.find(el => el.id === 6)).toMatchObject({
          priority: 3
        });
      });

      it("shifts up other items priority when item priority shift down", () => {
        const oldItem = list.find(el => el.id === 6);
        const item = { ...oldItem, priority: 2 };

        const result = TodosService.updateItemsPriority(list, item, oldItem);

        expect(result.find(el => el.id === 4)).toMatchObject({
          priority: 1
        });
        expect(result.find(el => el.id === 5)).toMatchObject({
          priority: 3
        });
      });
    });

    describe("And `done` flag have changed", () => {
      let oldItem, item, result;
      beforeAll(() => {
        oldItem = list.find(el => el.id === 1);
        item = { ...oldItem, done: true, priority: 2 };
        result = TodosService.updateItemsPriority(list, item, oldItem);
      });

      it("fills the void in the old flag items", () => {
        expect(result.find(el => el.id === 2)).toMatchObject({
          priority: 1
        });

        expect(result.find(el => el.id === 3)).toMatchObject({
          priority: 2
        });
      });
      it("creates space for the new item and shift up priorities of the rest", () => {
        expect(result.find(el => el.id === 4)).toMatchObject({
          priority: 1
        });
        expect(result.find(el => el.id === 5)).toMatchObject({
          priority: 3
        });
        expect(result.find(el => el.id === 6)).toMatchObject({
          priority: 4
        });
      });
    });
  });
});
