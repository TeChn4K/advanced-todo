const TodosService = {
  addItemTo(list = [], item) {
    const listUpdated = list.map(el => {
      // Need to increment priorities of each item with same `done` and >= priority
      const increment = el.done === item.done && el.priority >= item.priority;
      return {
        ...el,
        priority: el.priority + (increment ? 1 : 0)
      };
    });
    listUpdated.push(item);

    return listUpdated;
  },

  // @TODO: Do not replace the whole item, but only the properties given
  updateItemFrom(list = [], item) {
    return list.map(el => ({
      ...el,
      ...(el.id === item.id ? item : {})
    }));
  },

  /**
   * Update priorities of items in [list], compare to {item} and {oldItem}
   *
   * It does not update {item} in the {list}
   *
   * @param {Array} list The array of items to manage priorities
   * @param {Object} item The new item
   * @param {Object} oldItem The old item
   */
  updateItemsPriority(list = [], item, oldItem) {
    // `done` have not changed
    if (item.done === oldItem.done) {
      return list.map(el => {
        let increment = 0;
        // We only care about same `done` flag but not item itself
        if (el.id !== item.id && el.done === item.done) {
          // If priority go down
          if (
            item.priority < oldItem.priority &&
            el.priority >= item.priority &&
            el.priority < oldItem.priority
          ) {
            increment = 1;
          }
          // If priority go up
          else if (
            item.priority > oldItem.priority &&
            el.priority <= item.priority &&
            el.priority > oldItem.priority
          ) {
            increment = -1;
          }
        }
        return {
          ...el,
          priority: el.priority + increment
        };
      });
    }
    // `done` have changed
    else {
      return list.map(el => {
        let increment = 0;
        if (el.id !== item.id) {
          // items with same done flag have to do some space for new item
          if (el.done === item.done) {
            if (el.priority >= item.priority) {
              increment = 1;
            }
          }
          // items with old done flag have to reduce the free space
          else {
            if (el.priority > oldItem.priority) {
              increment = -1;
            }
          }
        }

        return {
          ...el,
          priority: el.priority + increment
        };
      });
    }
  },

  deleteItemFrom(list = [], item) {
    const oldItem = list.find(el => el.id === item.id);
    return list
      .filter(el => el.id !== oldItem.id)
      .map(el => {
        // Decrement priorities from item priority
        const decrement =
          el.done === oldItem.done && el.priority > oldItem.priority;
        return {
          ...el,
          priority: el.priority - (decrement ? 1 : 0)
        };
      });
  }
};

export default TodosService;
