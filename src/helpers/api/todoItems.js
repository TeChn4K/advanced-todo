import FetchService from "./fetch";

export default {
  async add(item) {
    return await FetchService.fetch("todo_items", {
      method: "POST",
      body: JSON.stringify(item)
    });
  },
  async update(item) {
    const id = item.id;
    const newItem = { ...item };
    delete newItem.id;

    return await FetchService.fetch(`todo_items?id=eq.${id}`, {
      method: "PATCH",
      body: JSON.stringify(newItem)
    });
  },
  async delete(id) {
    if (!id) {
      throw "itemId empty";
    }
    return await FetchService.fetch(`todo_items?id=eq.${id}`, {
      method: "DELETE"
    });
  }
};
