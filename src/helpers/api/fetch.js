import AuthService from "../auth";

class FetchService {
  constructor() {
    this.domain = "http://localhost:3000/";

    this.fetch = this.fetch.bind(this);
  }

  async fetch(uri, options = {}) {
    const headers = {
      "Content-Type": "application/json"
    };

    if (AuthService.isLogged()) {
      headers["Authorization"] = `Bearer ${AuthService.getToken()}`;
    }

    if (["POST", "PATCH", "DELETE"].indexOf(options.method) !== -1) {
      headers["Prefer"] = "return=representation";
    }

    let response;
    try {
      response = await fetch(this.domain + uri, {
        headers,
        ...options
      });
    } catch (error) {
      alert("Server unreachable");
      throw "Server unreachable";
    }

    const data = await response.json();
    this._checkStatus(data, response);

    return data;
  }

  _checkStatus(data, response) {
    if (response.ok !== true) {
      alert(
        `API error: (${response.status}) ${response.statusText}\n${
          data.message
        }`
      );
      var error = new Error(
        `(${response.status}) ${response.statusText}: ${data.message}`
      );
      error.response = response;
      throw error;
    }
  }
}

export default new FetchService();
