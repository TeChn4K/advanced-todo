import FetchService from "./fetch";

export default {
  async fetch() {
    return await FetchService.fetch(
      "todos?select=*,todo_items(*)&order=id&todo_items.order=done,priority"
    );
  },
  async add(item) {
    return await FetchService.fetch("todos", {
      method: "POST",
      body: JSON.stringify(item)
    });
  },
  async delete(id) {
    if (!id) {
      throw "id empty";
    }
    return await FetchService.fetch(`todos?id=eq.${id.toString()}`, {
      method: "DELETE"
    });
  }
};
