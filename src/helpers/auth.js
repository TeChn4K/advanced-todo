import FetchService from "./api/fetch";

class AuthService {
  constructor() {
    this.isLogged = this.isLogged.bind(this);
    this.login = this.login.bind(this);

    this.newTokenCallbacks = new Object();
    this.increment = 0;
  }

  async login(email, password) {
    const response = await FetchService.fetch("rpc/login", {
      method: "POST",
      body: JSON.stringify({ email, password })
    });

    const expire = new Date(response[0].token.expire);
    this.setTokenData(response[0].token.value, expire.toString());

    return expire;
  }

  async refreshToken() {
    const response = await FetchService.fetch("rpc/refresh_token", {
      method: "POST"
    });

    const expire = new Date(response.expire);
    this.setTokenData(response.value, expire.toString());

    return expire;
  }

  logout() {
    localStorage.removeItem("token");
    localStorage.removeItem("token_expire");
  }

  isLogged() {
    const token = this.getToken();
    return !!token && !this.isTokenExpired();
  }

  setTokenData(token, expire) {
    localStorage.setItem("token", token);
    localStorage.setItem("token_expire", expire);

    this.triggerNewToken();
  }

  getToken() {
    return localStorage.getItem("token");
  }

  isTokenExpired() {
    const tokenExpire = localStorage.getItem("token_expire");
    if (!tokenExpire || new Date(tokenExpire) < new Date()) {
      return true;
    }

    return false;
  }

  getExpirationDate() {
    const tokenExpire = localStorage.getItem("token_expire");
    if (!tokenExpire) {
      return false;
    }

    return new Date(tokenExpire);
  }

  onNewToken(fn) {
    this.newTokenCallbacks[++this.increment] = fn;
    return this.increment;
  }
  unNewToken(id) {
    delete this.newTokenCallbacks[id];
  }

  triggerNewToken() {
    Object.values(this.newTokenCallbacks).map(callback => callback());
  }
}

export default new AuthService();
