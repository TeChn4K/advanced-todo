# Advanced Todo

Advanced Todo is my React sandbox application

## Tools and libraries

**Front**

- `React` 16 & `react-router`
- Drag'n drop: `react-beautiful-dnd`
- Tests: `Jest` & `react-testing-library`
- Build, hot reload & dev server: `Webpack`
- Transpiler: `Babel`
- UI: `Bootstrap` & `styled-components`

**Back**

- DB: `PostgreSQL`
- RESTful API: `PostgREST`
- Auth: `JWT` (JSON Web Token)
- Docker containers

## Requirements

`docker`, `webpack` and `yarn` have to be installed on your system.

## Prepare to launch

1. Clone the project
2. Change defaults users/password and jwt secret in the `.env` file

## Launch!

1. Run the docker containers `docker-compose up -d`
2. Run the dev server `yarn run dev`
3. Open `http://localhost:8080/`

## Tests

1. Run all tests with `yarn run test`
