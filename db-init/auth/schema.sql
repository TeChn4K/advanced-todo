drop schema if exists auth cascade;
create schema auth;

SET search_path TO auth;

--
-- DB Roles
--

create type user_role as enum ('webuser');

-- role to connect only, without any privilege (only login)
drop role if exists :authenticator;
create role :"authenticator" with login password :'authenticator_pass';

GRANT CONNECT ON DATABASE :dbname TO :"authenticator";

-- requests that are not authenticated will be executed with this role's privileges
drop role if exists :"anonymous";
create role :"anonymous";
grant :"anonymous" to :"authenticator";

drop role if exists webuser;
create role webuser;
grant webuser to :"authenticator";


create type session as (me json, token json);


create or replace function encrypt_pass() returns trigger as $$
begin
  if new.password is not null then
  	new.password = public.crypt(new.password, public.gen_salt('bf'));
  end if;
  return new;
end
$$ language plpgsql;


create or replace function sign_jwt(json) returns text as $$
    select pgjwt.sign($1, settings.get('jwt_secret'))
$$ stable language sql;

create or replace function get_jwt_payload(json) returns json as $$
    select json_build_object(
      'role', $1->'role',
      'user_id', $1->'id',
      'exp', extract(epoch from now())::integer + settings.get('jwt_lifetime')::int
    )
$$ stable language sql;


create table "user" (
	id                   serial primary key,
	name                 text not null,
	email                text not null unique,
	"password"           text not null,
	"role"				       auth.user_role not null default settings.get('auth.default-role')::auth.user_role,

  check (length(name)>2),
	check (email ~* '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$')
);

create trigger user_encrypt_pass_trigger
  before insert or update on auth."user"
  for each row
  execute procedure auth.encrypt_pass();


INSERT INTO "user" (name, email, "password", "role") VALUES
('test user', 'test@test.com', 'password', settings.get('auth.default-role')::auth.user_role);
