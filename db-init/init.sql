\set dbname `echo "$DB_NAME"`
\set anonymous `echo "$DB_ANON_ROLE"`
\set authenticator `echo "$DB_USER"`
\set authenticator_pass `echo $DB_PASS`
\set jwt_secret `echo $JWT_SECRET`
\set quoted_jwt_secret '\'' :jwt_secret '\''


-- PUBLIC not welcome!
REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON DATABASE :dbname FROM PUBLIC;

ALTER DEFAULT PRIVILEGES REVOKE ALL ON TABLES FROM PUBLIC;
ALTER DEFAULT PRIVILEGES REVOKE ALL ON SEQUENCES FROM PUBLIC;
ALTER DEFAULT PRIVILEGES REVOKE ALL ON FUNCTIONS FROM PUBLIC;
ALTER DEFAULT PRIVILEGES REVOKE ALL ON TYPES FROM PUBLIC;



-- Store some secrets !
\ir settings/schema.sql
select settings.set('jwt_secret', :quoted_jwt_secret);
select settings.set('jwt_lifetime', '3600');
select settings.set('auth.default-role', 'webuser');

-- PgJWT install 
\ir pgjwt/schema.sql

-- DB roles and users auth
\ir auth/schema.sql


-- API schema
DROP schema if exists api cascade;
CREATE SCHEMA api;

SET search_path TO api;

REVOKE ALL ON SCHEMA api FROM PUBLIC;
GRANT USAGE ON SCHEMA api TO :"anonymous";
GRANT USAGE ON SCHEMA api TO webuser;

--
-- todo_items
--

CREATE TABLE todo_items (
    id integer NOT NULL,
    todo_id integer NOT NULL,
    priority integer default '1' NOT NULL,
    done boolean DEFAULT false NOT NULL,
    task text NOT NULL,
    due timestamp with time zone
);

ALTER TABLE ONLY todo_items
    ADD CONSTRAINT todo_items_pkey PRIMARY KEY (id);

GRANT SELECT ON TABLE todo_items TO :"anonymous";
GRANT ALL ON TABLE todo_items TO webuser;

CREATE SEQUENCE todo_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE todo_items_id_seq OWNED BY api.todo_items.id;
ALTER TABLE ONLY todo_items ALTER COLUMN id SET DEFAULT nextval('api.todo_items_id_seq'::regclass);

GRANT ALL ON SEQUENCE todo_items_id_seq TO webuser;



CREATE OR REPLACE function switch_todo() RETURNS trigger AS $$
    begin
        UPDATE todo_items A
        SET priority = priority + 1
        WHERE A.done = NEW.done
            AND A.priority >= NEW.priority
            AND A.id <> NEW.id
            AND A.todo_id = NEW.todo_id;

        UPDATE todo_items A
        SET priority = priority - 1
        WHERE A.done <> NEW.done
            AND A.priority > OLD.priority
            AND A.todo_id = NEW.todo_id;

        return NEW;
    end;
$$ language plpgsql;

CREATE TRIGGER shift_todo_update
    AFTER UPDATE OF priority ON todo_items
    FOR EACH ROW
    WHEN ((pg_trigger_depth() < 1) AND (OLD.done <> NEW.done))
    EXECUTE PROCEDURE switch_todo();



CREATE OR REPLACE function shift_priority() RETURNS trigger AS $$
    begin
        UPDATE todo_items A
        SET priority = priority + 1
        WHERE NEW.priority < OLD.priority
            AND A.priority >= NEW.priority
            AND A.priority < OLD.priority
            AND A.id <> NEW.id
            AND A.done = NEW.done
            AND A.todo_id = NEW.todo_id;

        UPDATE todo_items A
        SET priority = priority - 1
        WHERE NEW.priority > OLD.priority
            AND A.priority <= NEW.priority
            AND A.priority > OLD.priority
            AND A.id <> NEW.id
            AND A.done = NEW.done
            AND A.todo_id = NEW.todo_id;

        return NEW;
    end;
$$ language plpgsql;

CREATE TRIGGER shift_priority_update
    AFTER UPDATE OF priority ON todo_items
    FOR EACH ROW
    WHEN ((pg_trigger_depth() < 1) AND (OLD.priority <> NEW.priority) AND (OLD.done = NEW.done))
    EXECUTE PROCEDURE shift_priority();


CREATE OR REPLACE function shift_up_priority() RETURNS trigger AS $$
    begin
        UPDATE todo_items A
        SET priority = priority + 1
        WHERE A.priority >= NEW.priority
            AND A.id <> NEW.id
            AND A.done = NEW.done
            AND A.todo_id = NEW.todo_id;

        return NEW;
    end;
$$ language plpgsql;

CREATE TRIGGER shift_priority_insert
    AFTER INSERT ON todo_items
    FOR EACH ROW
    EXECUTE PROCEDURE shift_up_priority();


CREATE OR REPLACE function shift_down_priority() RETURNS trigger AS $$
    begin
        UPDATE todo_items A
        SET priority = priority - 1
        WHERE A.priority > OLD.priority
        AND A.done = OLD.done
            AND A.todo_id = OLD.todo_id;

        return NEW;
    end;
$$ language plpgsql;

CREATE TRIGGER shift_priority_delete
    AFTER DELETE ON todo_items
    FOR EACH ROW
    EXECUTE PROCEDURE shift_down_priority();



--
-- Todos
--

CREATE TABLE todos (
    id integer NOT NULL,
    title text NOT NULL
);

ALTER TABLE ONLY todos
    ADD CONSTRAINT todos_pkey PRIMARY KEY (id);

GRANT SELECT ON TABLE todos TO :"anonymous";
GRANT ALL ON TABLE todos TO webuser;


CREATE SEQUENCE todos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE todos_id_seq OWNED BY todos.id;
ALTER TABLE ONLY todos ALTER COLUMN id SET DEFAULT nextval('api.todos_id_seq'::regclass);

GRANT ALL ON SEQUENCE todos_id_seq TO webuser;


--
-- Foreign Keys
--

ALTER TABLE ONLY todo_items
    ADD CONSTRAINT todo_items_todo_id_fkey FOREIGN KEY (todo_id) 
    REFERENCES todos(id) ON DELETE CASCADE;


--
-- Datas !
--

INSERT INTO "todos" ("title") VALUES
('Todo list 1');




-- "SECURITY DEFINER" : the function runs as the owner of the function, rather
-- than whomever is calling it:
create or replace function login(email text, password text) returns auth.session as $$
declare
    usr record;
    usr_api record;
    result record;
begin

    EXECUTE format(
    ' select row_to_json(u.*) as j'
        ' from %I."user" as u'
        ' where u.email = $1 and u.password = public.crypt($2, u.password)'
    , quote_ident('auth'))
     INTO usr
     USING $1, $2;

    if usr is NULL then
        raise exception 'invalid email/password';
    else
        EXECUTE format(
            ' select json_populate_record(null::%I."user", $1) as r'
        , quote_ident('auth'))
         INTO usr_api
      USING usr.j;

        result = (
            row_to_json(usr_api.r),
            json_build_object(
                'value', auth.sign_jwt(auth.get_jwt_payload(usr.j)),
                'expire', now() + (settings.get('jwt_lifetime') || ' second')::interval
            )
        );
        return result;
    end if;
end
$$ stable security definer language plpgsql;

GRANT EXECUTE ON FUNCTION login TO :"anonymous";
GRANT EXECUTE ON FUNCTION login TO webuser;



create or replace function refresh_token() returns json as $$
declare
  usr record;
  result json;
    userid text;
begin

    if current_setting('request.jwt.claim.user_id', true) IS NULL then
      raise exception 'no userid in token';
    else

        EXECUTE format(
            ' select row_to_json(u.*) as j'
            ' from %I."user" as u'
            ' where u.id = $1'
            , quote_ident('auth'))
        INTO usr
        USING current_setting('request.jwt.claim.user_id', true)::int;

        if usr is NULL then
            raise exception 'user not found';
        else
            result = (
                json_build_object(
                    'value', auth.sign_jwt(auth.get_jwt_payload(usr.j)),
                    'expire', now() + (settings.get('jwt_lifetime') || ' second')::interval
                )
            );

            return result;
        end if;
    end if;
end
$$ stable security definer language plpgsql;

GRANT EXECUTE ON FUNCTION refresh_token TO webuser;